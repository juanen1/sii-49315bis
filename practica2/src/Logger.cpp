#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#define MAX_BUFFER 1024

int main(void)
{
	int fd;
	int p_j1, p_j2;
	char buffe[MAX_BUFFER];
	//Creamos la tuberia
	mkfifo("/tmp/mififo",0666);
	
	fd = open("/tmp/mififo", O_RDONLY);
	
	if(fd < 0)
	{
		perror("Error al crear la tuberia");
		return 0;
	}
	
	while (1){

		read(fd, buffe, sizeof(buffe));
		printf("%s\n", buffe);

	 	if (strcmp(buffe, "Fin del programa") == 0)
	 	{ 
	 	break;
		}
	close (fd);
	unlink("/tmp/mififo");

	return 0;
}
	
}
