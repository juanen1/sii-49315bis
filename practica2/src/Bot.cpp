#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>

#include "DatosMemCompartida.h"

int main(void)
{
	DatosMemCompartida *memoria;
	int fichbot; 
	
	fichbot = open("/tmp/Bot", O_RDWR);
	memoria = (DatosMemCompartida*) mmap(NULL, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fichbot, 0);	
	close(fichbot);
	
	while(1)
	{
		if (memoria->accion1 == 2) break;

		if (memoria->raqueta1.y1 < memoria->esfera.centro.y)
		{
			memoria->accion1=1;
		}
		else if (memoria->raqueta1.y2 > memoria->esfera.centro.y) 
		{
			memoria->accion1=-1;
		}
		else
		{
			memoria->accion1=0;
		}

		if (memoria->inactivo >= 400)
		{
			if (memoria->raqueta2.y1 < memoria->esfera.centro.y) 
			{
				memoria->accion2=1;
			}
			else if (memoria->raqueta2.y2 > memoria->esfera.centro.y) 
			{
				memoria->accion2=-1;
			}
			else 
			{
				memoria->accion2=0;
			}
		}
		
		usleep(25000);
	}
	
	printf("Fin del programa\n");
	munmap(memoria, sizeof(DatosMemCompartida));

	return 0;
}

	
